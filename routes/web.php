<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "HomeController@index");
Route::post("send-message", "Admin\MessageController@send");

Route::get("/file/{file}", "FileController@show")->where("file", "(.+)");
Route::get("/download/{file}", "FileController@download")->where("file", "(.+)");

Route::resource("news", "NewsController", ["only" => ["index", "show"]]);
Route::resource("gallery", "GalleryController", ["only" => ["index", "show"]]);
// Route::get("message-mailtest/{id}", "Admin\MessageController@testMail");

Route::prefix("admin")->group(function() {
    Auth::routes();

    Route::namespace("Admin")->middleware("auth")->group(function() {
        // Route::get("/", "HomeController@index");
        // Route::get("home", "HomeController@index");
        Route::get("/", "MessageController@inbox");
        Route::get("message", "MessageController@inbox");
        Route::get("message/read-all", "MessageController@readAll");
        Route::get("message/{id}", "MessageController@show");
        Route::post("message/{id}/reply", "MessageController@reply");

        Route::get("setting", "SettingController@index");
        Route::post("account", "SettingController@account");
        Route::post("change-password", "SettingController@changePassword");

        Route::resource("news", "NewsController");
        Route::resource("gallery", "GalleryController");
    });
});
