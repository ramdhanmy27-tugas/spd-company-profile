<?php

if (!function_exists("asset_file")) {
    function asset_file($filepath) {
        return url("file/$filepath");
    }
}

if (!function_exists("flash")) {
    function flash($message, $variant = "info") {
        $messages = Session::get("msg.$variant", []);

        if (is_array($message)) {
            $messages = array_merge($messages, $message);
        }
        else {
            $messages[] = $message;
        }

        Session::flash("msg.$variant", $messages);
    }
}
