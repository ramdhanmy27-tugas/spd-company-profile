<?php

namespace App\Exceptions;

use Illuminate\Contracts\Validation\Validator;

class ModelValidationException extends \Exception {

    private $validator;

    public function __construct(Validator $validator) {
        $this->validator = $validator;
        $this->message = $this->validator->errors()->first();
    }

    public function getValidator() {
        return $this->validator;
    }
}
