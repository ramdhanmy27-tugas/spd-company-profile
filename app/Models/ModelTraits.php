<?php

namespace App\Models;

use App\Exceptions\ModelValidationException;
use Validator;

trait ModelTraits {

    // public function __construct(array $attributes) {
    //     $defaultValues = method_exists(self::class, "defaultValues") ? self::defaultValues() : [];

    //     parent::__construct(array_merge($defaultValues, $attributes));
    // }

    public static function label($key = null, $default = null) {
        $labels = method_exists(self::class, "defaultLabel") ? self::defaultLabel() : [];

        return array_get($labels, $key, $default === null ? title_case($key) : $default);
    }

    public function updateOrFail(array $data) {
        $rules = method_exists(self::class, "rules") ? self::rules() : [];
        $validator = Validator::make($data, array_only($rules, array_keys($data)));

        if ($validator->fails()) {
            throw new ModelValidationException($validator);
        }

        return $this->update($data);
    }

    public static function createOrFail(array $data) {
        self::validateOrFail($data);

        return self::create($data);
    }

    public static function fillOrFail(array $data) {
        self::validateOrFail($data);

        return new self($data);
    }

    public static function validate(array $data) {
        $rules = method_exists(self::class, "rules") ? self::rules() : [];
        $defaultValues = method_exists(self::class, "defaultValues") ? self::defaultValues() : [];

        return Validator::make(array_merge($defaultValues, $data), $rules);
    }

    public static function validateOrFail(array $data) {
        $validator = self::validate($data);

        if ($validator->fails()) {
            throw new ModelValidationException($validator);
        }
    }

    /* Overrides */

    public function fill(array $attributes) {
        $defaultValues = method_exists(self::class, "defaultValues") ? self::defaultValues() : [];
        $values = array_merge($defaultValues, $attributes);

        if (method_exists($this, "beforeFill")) {
            $this->beforeFill($values);
        }

        $return = parent::fill($values);

        if (method_exists($this, "afterFill")) {
            $this->afterFill($values);
        }

        return $return;
    }

    public function save(array $options = []) {
        if (method_exists($this, "beforeSave")) {
            $this->beforeSave($options);
        }

        $return = parent::save($options);

        if (method_exists($this, "afterSave")) {
            $this->afterSave($options);
        }

        return $return;
    }

    public function delete() {
        if (method_exists($this, "beforeDelete")) {
            $this->beforeDelete();
        }

        $return = parent::delete();

        if (method_exists($this, "afterDelete")) {
            $this->afterDelete();
        }

        return $return;
    }
}
