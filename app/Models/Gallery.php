<?php

namespace App\Models;

use App\Models\ModelTraits;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;

class Gallery extends Model {

    use ModelTraits;

    const IMAGE_DIR = "public/gallery";

    protected $table = 'gallery';

    protected $fillable = ['image', 'description', 'user_id'];

    public static function rules() {
        return [
            'image' => 'required|image',
            'description' => 'required|string',
            'user_id' => 'required|integer',
        ];
    }

    public static function defaultLabel() {
        return [
            'id' => 'ID',
            'image' => 'Gambar',
            'description' => 'Deskripsi',
            'user_id' => 'User',
            'created_at' => 'Dibuat',
            'updated_at' => 'Dirubah',
        ];
    }

    public function beforeSave() {
        // upload images if exists
        if ($this->attributes["image"] instanceof UploadedFile) {
            $path = storage_path("app/".self::IMAGE_DIR);
            $filename = Uuid::uuid1().".".$this->attributes["image"]->extension();
            $img = Image::make($this->attributes["image"]);

            // large image
            $img->resize(800, 800, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save("$path/$filename");

            // small image
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save("$path/sm-$filename");

            // save filename into database
            $this->image = $filename;
        }
    }

    public function beforeDelete() {
        Storage::delete(self::IMAGE_DIR."/$this->image");
    }

    public function getImageUrlAttribute($size = null) {
        $file = self::IMAGE_DIR."/".($size === null ? "" : "$size-").$this->attributes["image"];

        return asset_file(Storage::exists($file) ? preg_replace("/^public\//", "", $file) : "gallery/default.png");
    }

    public function scopeAuth($query, $user_id = null) {
        if ($user_id === null) {
            $user_id = Auth::user()->id;
        }

        return $query->where("user_id", $user_id);
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
