<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable {

    use ModelTraits;

    protected $fillable = [
        "name",
        "email",
        "password",
    ];

    protected $hidden = ['password'];

    public $timestamps = false;

    public static function rules() {
        return [
            "name" => "required|string|max:255",
            "email" => "required|string|max:255",
            "password" => "nullable|string|min:6|confirmed",
        ];
    }

    public static function defaultLabel() {
        return [
            "name" => "Nama",
            "email" => "Email",
            "password" => "Password",
            "old_password" => "Password Lama",
            "new_password" => "Password Baru",
            "confirm_password" => "Konfirmasi Password",
        ];
    }

    public function beforeSave() {
        // dd("USER PASSWORD IS DIRTY: ", $this->isDirty("password"));
        if ($this->password && $this->isDirty("password")) {
            // dd($this->password);
            $this->password = Hash::make($this->password);
        }
        else unset($this->password);
    }

    public function setAttribute($key, $value) {
        if ($key != $this->getRememberTokenName()) {
            parent::setAttribute($key, $value);
        }
    }
}
