<?php

namespace App\Models;

use App\Models\ModelTraits;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;

class News extends Model {

    use ModelTraits;

    const IMAGE_DIR = "public/news";

    protected $table = 'news';

    protected $fillable = ['title', 'content', 'cover', 'user_id'];

    public static function rules() {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'cover' => 'required|image',
            'user_id' => 'required|integer',
        ];
    }

    public static function defaultLabel() {
        return [
            'id' => 'ID Berita',
            'title' => 'Judul',
            'content' => 'Konten',
            'cover' => 'Sampul',
            'user_id' => 'User',
            'created_at' => 'Dibuat',
            'updated_at' => 'Dirubah',
        ];
    }

    public function beforeSave() {
        // upload images if exists
        if ($this->attributes["cover"] instanceof UploadedFile) {
            $path = storage_path("app/".self::IMAGE_DIR);
            $filename = Uuid::uuid1().".".$this->attributes["cover"]->extension();
            $img = Image::make($this->attributes["cover"]);

            // large image
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save("$path/$filename");

            // small image
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save("$path/sm-$filename");

            // save filename into database
            $this->cover = $filename;
        }
    }

    public function beforeDelete() {
        Storage::delete(self::IMAGE_DIR."/$this->cover");
    }

    public function getCoverUrlAttribute($size = null) {
        $file = self::IMAGE_DIR."/".($size === null ? "" : "$size-").$this->attributes["cover"];

        return asset_file(Storage::exists($file) ? preg_replace("/^public\//", "", $file) : "news/default.png");
    }

    public function getSeoIdAttribute() {
        return "$this->id-".str_replace("_", "-", snake_case($this->title));
    }

    public function getShortContentAttribute($length = null) {
        $content = strip_tags($this->content);
        $length = $length === null ? 200 : $length;

        return strlen($content) > $length ? substr($content, 0, $length)."..." : $content;
    }

    public function scopeSearch($query, $search = "") {
        if (strlen($search) > 0) {
            $query->where(DB::raw("lower(title)"), "like", "%".strtolower($search)."%");
        }

        return $query;
    }

    public function scopeAuth($query, $user_id = null) {
        if ($user_id === null) {
            $user_id = Auth::user()->id;
        }

        return $query->where("user_id", $user_id);
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
