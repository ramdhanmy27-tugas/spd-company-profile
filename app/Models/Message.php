<?php

namespace App\Models;

use App\Models\ModelTraits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Message extends Model {

    use ModelTraits;

    protected $table = 'message';

    protected $fillable = ['name', 'email', 'subject', 'body', 'is_read', 'received_at', 'parent_id', 'user_id'];

    public $timestamps = false;

    protected $attributes = ["is_read" => false];

    public static function rules() {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|string|max:255',
            'subject' => 'required|string|max:255',
            'body' => 'required|string',
            'is_read' => 'required|boolean',
            'received_at' => 'required|date',
        ];
    }

    public static function defaultLabel() {
        return [
            'id' => 'ID Pesan',
            'subject' => 'Subjek',
            'name' => 'Nama Pengirim',
            'email' => 'Email',
            'body' => 'Pesan',
            'received_at' => 'Diterima',
        ];
    }

    public function beforeSave() {
        $this->received_at = date("Y-m-d H:i:s");
    }

    public function scopeSearch($query, $search = "") {
        if (strlen($search) > 0) {
            $query->where(DB::raw("lower(email)"), "like", "%".strtolower($search)."%")
                ->orWhere(DB::raw("lower(body)"), "like", "%".strtolower($search)."%");
        }

        return $query;
    }

    public function scopeIsRead($query, $is_read = true) {
        return $query->where("is_read", $is_read);
    }
}
