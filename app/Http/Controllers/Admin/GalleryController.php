<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller {

    public function index() {
        $data = Gallery::auth(Auth::user()->id)->orderBy("created_at", "desc")->paginate();

        return view("admin.gallery.list", compact("data"));
    }

    public function create() {
        $model = [];

        return view("admin.gallery.add", compact("model"));
    }

    public function store(Request $request) {
        Gallery::createOrFail(array_merge($request->all(), [
            "user_id" => Auth::user()->id,
        ]));

        return redirect("admin/gallery");
    }

    public function edit($id) {
        $model = Gallery::findOrFail($id);

        return view("admin.gallery.edit", compact("model"));
    }

    public function update(Request $request, $id) {
        Gallery::findOrFail($id)->updateOrFail($request->all());

        return redirect("admin/gallery");
    }

    public function destroy($id) {
        Gallery::findOrFail($id)->delete($id);

        return redirect("admin/gallery");
    }
}
