<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller {

    public function index() {
        $data = User::orderBy("name")->paginate();

        return view("admin.user.list", compact("data"));
    }

    public function create() {
        $model = [];

        return view("admin.user.add", compact("model"));
    }

    public function store(Request $request) {
        User::createOrFail($request->all());

        return redirect("admin/user");
    }

    public function edit($id) {
        $model = User::findOrFail($id);

        return view("admin.user.edit", compact("model"));
    }

    public function update(Request $request, $id) {
        User::findOrFail($id)->updateOrFail($request->all());

        return redirect("admin/user");
    }

    public function destroy($id) {
        User::findOrFail($id)->delete($id);

        return redirect("admin/user");
    }
}
