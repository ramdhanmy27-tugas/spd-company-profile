<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller {

    public function index() {
        $data = News::auth(Auth::user()->id)->orderBy("created_at", "desc")->paginate();

        return view("admin.news.list", compact("data"));
    }

    public function create() {
        $model = [];

        return view("admin.news.add", compact("model"));
    }

    public function store(Request $request) {
        News::createOrFail(array_merge($request->all(), [
            "user_id" => Auth::user()->id,
        ]));

        return redirect("admin/news");
    }

    public function edit($id) {
        $model = News::findOrFail($id);

        return view("admin.news.edit", compact("model"));
    }

    public function update(Request $request, $id) {
        News::findOrFail($id)->updateOrFail($request->all());

        return redirect("admin/news");
    }

    public function destroy($id) {
        News::findOrFail($id)->delete($id);

        return redirect("admin/news");
    }
}
