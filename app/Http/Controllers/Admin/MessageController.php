<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\MessageSent;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    public function inbox(Request $req)
    {
        $data = Message::search($req->input("search"))
            ->whereNull("parent_id")
            ->orderBy("received_at", "desc")
            ->paginate(20);

        return view("admin.message.inbox", compact("data"));
    }

    public function show($id)
    {
        $message = Message::findOrFail($id);
        $childMessages = Message::where("parent_id", $id)
            ->orderBy("received_at")
            ->get();

        if (!$message->is_read) {
            $message->update(["is_read" => true]);
        }

        return view("admin.message.show", compact("message", "childMessages"));
    }

    public function readAll()
    {
        Message::isRead(false)->update(["is_read" => true]);

        return back();
    }

    public function send(Request $req)
    {
        try {
            DB::beginTransaction();
            $message = Message::createOrFail([
                "received_at" => date("Y-m-d H:i:s"),
                "is_read" => false,
            ] + $req->all());

            Mail::to(config("mail.from.address"))->send(new MessageSent($message));
            flash("Pesan anda telah terkirim.", "success");

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            flash("Pesan anda gagal terkirim.", "error");
        }

        return redirect("/#section-contact");
    }

    public function reply(Request $req, $id)
    {
        try {
            DB::beginTransaction();
            $user = Auth::user();
            $parent_msg = Message::findOrFail($id);
            $message = Message::createOrFail([
                "parent_id" => $id,
                "user_id" => $user->id,
                "name" => $user->name,
                "email" => $user->email,
                "subject" => "Re: $parent_msg->subject",
                "body" => $req->body,
                "received_at" => date("Y-m-d H:i:s"),
                "is_read" => true,
            ]);

            Mail::to($parent_msg->email)->send(new MessageSent($message));
            flash("Pesan anda telah terkirim.", "success");

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            flash("Pesan anda gagal terkirim.", "error");
        }

        return back();
    }

    public function testMail($id)
    {
        $message = Message::findOrFail($id);

        return view("email.message", ["msg" => $message]);
    }
}
