<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index(Request $req)
    {
        $user = Auth::user();

        return view("admin.setting.index", compact("user"));
    }

    public function changePassword(Request $req) {
        $user = Auth::user();

        if (Hash::check($req->old_password, $user->password)) {
            $validator = Validator::make($req->input(), [
                'old_password' => 'required|string',
                'new_password' => 'required|string|min:6',
            ]);

            if ($validator->fails()) {
                flash($validator->errors()->first(), "danger");
                return back();
            }

            if ($req->new_password == $req->confirm_password) {
                // $user->password = Hash::make($req->new_password);
                $user->password = $req->new_password;
                $user->save();

                flash("Password anda telah diganti.", "success");
            }
            else {
                flash("Konfirmasi password tidak valid.", "danger");
            }
        }
        else {
            flash("Gagal mengganti password", "danger");
        }

        return back();
    }

    public function account(Request $req)
    {
        Auth::user()->update($req->only(["name", "email"]));
        flash("Data akun anda telah diperbarui.", "success");

        return back();
    }
}
