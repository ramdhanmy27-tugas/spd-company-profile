<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller {

    public function index() {
        $data = Gallery::orderBy("created_at", "desc")->paginate(12);

        return view("gallery.list", compact("data"));
    }

    public function show($id) {
        $model = Gallery::findOrFail($id);

        return view("gallery.show", compact("model"));
    }
}
