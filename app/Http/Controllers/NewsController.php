<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller {

    public function index(Request $req) {
        $data = News::search($req->search)->orderBy("created_at", "desc")->paginate(5);
        $newest = News::orderBy("created_at", "desc")->take(5)->get();

        return view("news.list", compact("data", "newest"));
    }

    public function show($seo_id) {
        preg_match_all("/(\d+)\-.+/", $seo_id, $match);
        $model = News::findOrFail($match[1][0]);

        if ($model->seo_id !== $seo_id) {
            return abort(404);
        }

        return view("news.show", compact("model"));
    }
}
