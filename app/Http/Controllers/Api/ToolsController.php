<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tools;
use Illuminate\Http\Request;

class ToolsController extends Controller {

    public function list(Request $req) {
        return Tools::orderBy("part_name")
            ->search($req->input("search"))
            ->paginate($req->input("pageSize", 20));
    }

    public function show($id) {
        $tool = Tools::findOrFail($id);
        $tool->picture = $tool->getPictureUrlAttribute();

        return $tool;
    }
}
