<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use Illuminate\Http\Request;

class FileController extends Controller {

    public function show(Request $req, $path) {
        return response()->file($this->getFileIfExists($path));
    }

    public function download(Request $req, $path) {
        return response()->download($this->getFileIfExists($path));
    }

    private function getFileIfExists($req_file) {
        $filepath = storage_path("app/public/$req_file");

        if (!file_exists($filepath)) {
            abort(404);
        }

        return $filepath;
    }
}
