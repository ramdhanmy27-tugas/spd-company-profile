
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import path from 'path';
import v from 'voca';
// import $ from 'jquery';
import uri from 'urijs';
import Paginate from 'vuejs-paginate';

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('paginate', Paginate);

// import vue components
var req = require.context("./components", true, /^(.*\.(vue$))[^.]*$/im);

req.keys().forEach(function(file) {
  var filename = path.basename(file);
  var name = filename.substring(0, filename.lastIndexOf('.') + 1);

  Vue.component(v.kebabCase(name), req(`${file}`))
});

const app = new Vue({
  el: '#app'
});
