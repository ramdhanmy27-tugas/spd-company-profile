<?php

use App\Models\Peminjaman;

?>

@if ($status === Peminjaman::STATUS_REQ_BORROWED)
  <span class="label label-default">Request Peminjaman</span>
@elseif ($status === Peminjaman::STATUS_BORROWED)
  <span class="label label-warning">Dipinjam</span>
@elseif ($status === Peminjaman::STATUS_REQ_RETURNED)
  <span class="label label-info">Request Pengembalian</span>
@elseif ($status === Peminjaman::STATUS_RETURNED)
  <span class="label label-success">Dikembalikan</span>
@elseif ($status === Peminjaman::STATUS_REJECTED)
  <span class="label label-danger">Ditolak</span>
@endif
