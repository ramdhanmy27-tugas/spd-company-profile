<?php

use App\Models\Peminjaman;

?>

<div class="row">
  <div class="col-sm-6">
    <div class="table-responsive bg-white ba b--moon-gray br2">
      <table class="table table-hover table-border table-striped">
        <tr>
          <th class="tr">{{ Peminjaman::label("kode_peminjaman") }} :</th>
          <td>{{ $peminjaman->kode_peminjaman }}</td>
        </tr>
        <tr>
          <th class="tr">User :</th>
          <td>{{ $peminjaman->nik }} - {{ $peminjaman->user->name }}</td>
        </tr>
        <tr>
          <th class="tr">{{ Peminjaman::label("tgl_pinjam") }} :</th>
          <td>{{ date("d F Y", strtotime($peminjaman->tgl_pinjam)) }}</td>
        </tr>
        <tr>
          <th class="tr">{{ Peminjaman::label("tgl_kembali") }} :</th>
          <td>{{ $peminjaman->tgl_kembali === null ? "-" : date("d F Y", strtotime($peminjaman->tgl_kembali)) }}</td>
        </tr>
        <tr>
          <th class="tr">{{ Peminjaman::label("status") }} :</th>
          <td>
            @include("peminjaman.status-pinjam", ["status" => $peminjaman->status])
          </td>
        </tr>
      </table>
    </div>
  </div>
  <div class="col-sm-6">
    @each("peminjaman.tool", $peminjaman->tools(), "tool")
  </div>
</div>
