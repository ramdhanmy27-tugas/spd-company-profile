<?php

use App\Models\Tools;

?>

<div class="br2 pa3 ba b--moon-gray bg-white w-100 mb3 shadow-hover">
  <div class="dt w-100">
    <div class="w-30 dtc tc br2 v-mid overflow-hidden ba b--moon-gray">
      <img src="{{ $tool->picture_url }}" style="max-height: 70px; max-width: 100%; width: auto" />
    </div>

    <div class="w-70 dtc ph3">
      <p class="f2 bb b--moon-gray"> {{ $tool->part_name }} </p>
      <p class="f4 black-60">
        <span class="mr4"> <b>No Part:</b> {{ $tool->part_number }} </span>
        <span class="mr4"> <b>Lokasi:</b> {{ $tool->location }} </span>
      </p>
    </div>
  </div>
</div>
