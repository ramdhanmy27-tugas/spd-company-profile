<?php

use App\Models\Peminjaman;

?>

<div class="panel panel-default">
  <div class="panel-body">
    <div class="col-sm-6">
      <div class="b f3">{{ $peminjaman->kode_peminjaman }}</div>

      <div class="black-60 f4">
        <div>{{ $peminjaman->nik }} - {{ $peminjaman->user->name }}</div>
        <div>{{ date("d F Y", strtotime($peminjaman->tgl_pinjam)) }}</div>
      </div>
    </div>

    <div class="col-sm-6 tr">
      <div class="mb3">
        @include("peminjaman.status-pinjam", ["status" => $peminjaman->status])
      </div>

      @if ($peminjaman->borrowUrl && $peminjaman->status === Peminjaman::STATUS_REQ_BORROWED)
        <action
          href="{{ $peminjaman->borrowUrl }}"
          method="POST"
          cls="btn btn-xs btn-warning mb2"
          onClick="return confirm('Anda Yakin ?')"
        >
          <i class="glyphicon glyphicon-circle-arrow-right"></i> Pinjam
        </action>
      @endif

      @if ($peminjaman->returnUrl && $peminjaman->status === Peminjaman::STATUS_REQ_RETURNED)
        <action
          href="{{ $peminjaman->returnUrl }}"
          method="POST"
          cls="btn btn-xs btn-success mb2"
          onClick="return confirm('Anda Yakin ?')"
        >
          <i class="glyphicon glyphicon-circle-arrow-left"></i> Kembalikan
        </action>
      @endif

      @if ($peminjaman->reqReturnUrl && $peminjaman->status === Peminjaman::STATUS_BORROWED))
        <action
          href="{{ $peminjaman->reqReturnUrl }}"
          method="POST"
          cls="btn btn-xs btn-success mb2"
          onClick="return confirm('Anda Yakin ?')"
        >
          <i class="glyphicon glyphicon-circle-arrow-left"></i> Kembalikan
        </action>
      @endif

      @if ($peminjaman->borrowUrl && $peminjaman->status === Peminjaman::STATUS_REQ_BORROWED)
        <action
          href="{{ $peminjaman->borrowUrl }}"
          method="POST"
          cls="btn btn-xs btn-danger mb2"
          onClick="return confirm('Anda Yakin ?')"
        >
          <i class="glyphicon glyphicon-remove"></i> Tolak
        </action>
      @endif

      @if ($peminjaman->detailUrl)
        <action href="{{ $peminjaman->detailUrl }}" cls="btn btn-xs btn-info mb2">
          <i class="glyphicon glyphicon-list-alt"></i> Detail
        </action>
      @endif
    </div>
  </div>
</div>
