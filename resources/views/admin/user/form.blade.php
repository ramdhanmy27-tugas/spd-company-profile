<?php

use App\Models\User;

?>

<div class="form-group row">
  {!! Form::label("nik", User::label("nik"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    {!! Form::text("nik", null, ["class" => "form-control"]) !!}
  </div>
</div>

<div class="form-group row">
  {!! Form::label("name", User::label("name"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    {!! Form::text("name", null, ["class" => "form-control"]) !!}
  </div>
</div>

<div class="form-group row">
  {!! Form::label("password", User::label("password"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    {!! Form::password("password", ["class" => "form-control"]) !!}
  </div>
</div>

<div class="form-group row">
  {!! Form::label("password_confirmation", User::label("password_confirmation"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    {!! Form::password("password_confirmation", ["class" => "form-control"]) !!}
  </div>
</div>

<div class="form-group row">
  {!! Form::label("roles", User::label("roles"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    @foreach(User::roles() as $role => $role_name)
      <div>
        <label class="col-form-label pointer">
          {!! Form::checkbox("roles[]", $role) !!}
          {{ $role_name }}
        </label>
      </div>
    @endforeach
  </div>
</div>

<div class="form-group row">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-secondary">Simpan</button>
  </div>
</div>

@push("js")
  <script type="text/javascript" src="{{ asset("js/jquery.mask.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $("[name='nik']").mask("000-000");
    });
  </script>
@endpush
