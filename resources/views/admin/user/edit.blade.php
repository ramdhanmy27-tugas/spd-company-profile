@extends("admin.app")

@section("page-title")
  User <span class="text-primary">{{ $model->name }}</span>
@endsection

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("admin/user") }}" class="btn btn-default">
      <i class="glyphicon glyphicon-share-alt"></i> Kembali
    </a>
  </div>

  {!! Form::model($model, ["url" => url("admin/user/$model->nik"), "method" => "PUT"]) !!}
    @include("admin.user.form", get_defined_vars())
  {!! Form::close() !!}
@endsection
