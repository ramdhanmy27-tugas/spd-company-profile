@extends("admin.app")

@section("page-title", "Tambah User")

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("admin/user") }}" class="btn btn-default">
      <i class="glyphicon glyphicon-share-alt"></i> Kembali
    </a>
  </div>

  {!! Form::model([], ["url" => url("admin/user")]) !!}
    @include("admin.user.form", get_defined_vars())
  {!! Form::close() !!}
@endsection
