@extends("admin.app")

<?php

use App\Models\User;

?>

@section("page-title", "User")

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("admin/user/create") }}" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i> Tambah
    </a>
  </div>

  @if(count($data) > 0)
    <div class="table-responsive">
      <table class="table table-hover table-bordered bg-white">
        <thead>
          <tr>
            <th>{{ User::label("nik") }}</th>
            <th>{{ User::label("name") }}</th>
            <th>{{ User::label("roles") }}</th>
            <th width="170px"></th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $item)
            <tr>
              <td>{{ $item->nik }}</td>
              <td>{{ $item->name }}</td>
              <td>{{ $item->roles ? implode(", ", $item->roles) : "" }}</td>
              <td>
                <action href="{{ url("admin/user/$item->nik/edit") }}" cls="btn btn-sm btn-info mh1">
                  <i class="glyphicon glyphicon-edit white"></i> Edit
                </action>
                <action href="{{ url("admin/user/$item->nik") }}"
                  cls="btn btn-sm btn-danger mh1"
                  method="DELETE"
                  onClick="return confirm('Anda yakin ?')"
                >
                  <i class="glyphicon glyphicon-remove white"></i> Delete
                </action>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>

      {{ $data->links() }}
    </div>
  @else
    @include("utils.empty")
  @endif
@endsection
