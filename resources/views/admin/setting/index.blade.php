@extends("admin.app")

<?php

use App\Models\User;

?>

@section("page-title", "Pengaturan")

@section("content")
  <div class="panel panel-default">
    <div class="panel-heading b">Informasi Akun</div>
    <div class="panel-body">
      {{ Form::model($user, ["method" => "POST", "url" => url("admin/account") ]) }}
        <div class="form-group row">
          <div class="col-md-2 tr">
            {{ Form::label("name", User::label("name")) }}
          </div>
          <div class="col-md-6">
            {{ Form::text("name", null, ["class" => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-2 tr">
            {{ Form::label("email", User::label("email")) }}
          </div>
          <div class="col-md-6">
            {{ Form::text("email", null, ["class" => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-offset-2 col-md-6">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading b">Ganti Password</div>
    <div class="panel-body">
      {{ Form::model($user, ["method" => "POST", "url" => url("admin/change-password") ]) }}
        <div class="form-group row">
          <div class="col-md-2 tr">
            {{ Form::label("old_password", User::label("old_password")) }}
          </div>
          <div class="col-md-6">
            {{ Form::password("old_password", ["class" => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-2 tr">
            {{ Form::label("new_password", User::label("new_password")) }}
          </div>
          <div class="col-md-6">
            {{ Form::password("new_password", ["class" => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-2 tr">
            {{ Form::label("confirm_password", User::label("confirm_password")) }}
          </div>
          <div class="col-md-6">
            {{ Form::password("confirm_password", ["class" => "form-control"]) }}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-offset-2 col-md-6">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
