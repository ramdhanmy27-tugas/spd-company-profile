@extends("layout")

@section("body")
  <div class="container-fluid h-100">
    <div class="row h-100">
      <?php
        $path = Request::path();
        $menus = [
          // "admin/home" => [ "label" => "Beranda", "icon" => "glyphicon glyphicon-home" ],
          "admin/news" => [ "label" => "Berita", "icon" => "glyphicon glyphicon-list-alt" ],
          "admin/gallery" => [ "label" => "Galeri", "icon" => "glyphicon glyphicon-picture" ],
        ];
      ?>

      {{-- Sidebar Menu --}}
      <div class="col-sm-2 h-100 tr bg-white shadow-1 overflow-x-hidden overflow-y-auto">
        <a href="{{ url("admin") }}" class="pa4 dib row bb b--moon-gray">
          <img src="{{ url("img/logo.png") }}" width="100%" />
        </a>

        <ul class="list cf pa0 mt4">
          <li>
            <a href="{{ url("admin/message") }}" class="pv2 ph3 db gray">
              @if ($new_msg_count > 0) <span class="badge">{{ $new_msg_count }}</span> @endif
              Pesan
              <i class="glyphicon glyphicon-envelope" style="width: 25px"></i>
            </a>
          </li>

          @foreach ($menus as $url => $menu)
            <?php $is_active = strpos($path, $url) === 0 || ($path == "admin" && $url == "admin/home"); ?>

            <li>
              <a href="{{ url($url) }}" class="pv2 ph3 db @if($is_active) text-primary b @else gray @endif">
                {{ $menu["label"] }} <i class="{{ $menu["icon"] }} f4" style="width: 25px"></i>
              </a>
            </li>
          @endforeach

          <div class="row bb b--light-gray mv3"></div>
          <li>
            <a href="{{ url("admin/setting") }}" class="pv2 ph3 db gray">
              Pengaturan <i class="glyphicon glyphicon-cog" style="width: 25px"></i>
            </a>
          </li>

          <li>
            <action href="{{ url("admin/logout") }}" method="POST" cls="pv2 ph3 db gray bw0 bg-white pa0">
              Logout <i class="glyphicon glyphicon-log-out tr" style="width: 25px"></i>
            </action>
          </li>

          <div class="row bb b--light-gray mv3"></div>
          <li>
            <a href="{{ url("/") }}" class="pv2 ph3 db gray">
              Halaman Depan <i class="glyphicon glyphicon-circle-arrow-left" style="width: 25px"></i>
            </a>
          </li>
        </ul>
      </div>

      <div class="col-sm-10 h-100 overflow-y-auto">
        @if(Session::has("msg"))
          <div class="mt3">
            @foreach(Session::pull("msg") as $variant => $messages)
              <div class="alert alert-{{ $variant }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>

                <div class="list">
                  @foreach($messages as $message)
                    <li>{{ $message }}</li>
                  @endforeach
                </div>
              </div>
            @endforeach
          </div>
        @endif

        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>

            <p class="pa0 ma0 b">Terdapat kesalahan:</p>
            {!! implode('', $errors->all('<li class="f4">:message</li>')) !!}
          </div>
        @endif

        @hasSection("page-title")
          <h1 class="my-4 pb2 bb b--moon-gray">
            @yield("page-title")
          </h1>
        @endif

        @yield("content")
      </div>
    </div>
  </div>
@endsection
