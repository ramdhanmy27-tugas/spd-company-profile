@extends("admin.app")

<?php

use App\Models\Message;

?>

@section("page-title")
  <div >{{ $message->subject }}</div>
  <div class="f4 mt3">
    <span>{{ $message->name }}</span>
    <a href="mailto:{{ $message->email }}">{{ $message->email }}</a>
  </div>
@endsection

@section("content")
  <div class="mb3 tr" style="margin-top: -80px">
    <a href="{{ url("admin/message") }}" class="btn btn-default">
      <i class="glyphicon glyphicon-share-alt"></i> Kembali
    </a>
    <div class="tr f4 silver mt1 i">
      {{ date("d M Y, H:i", strtotime($message->received_at)) }}
    </div>
  </div>

  <div class="overflow-hidden">
    <pre class="w-100 f4 sans-serif bg-white pa3 mb3 br3 ba b--moon-gray"
      style="word-break: break-word; white-space: pre-wrap;">{{ $message->body }}</pre>

    @foreach($childMessages as $msg)
      <div class="f4 bg-white ph3 mb3 ml4 br3 ba b--moon-gray">
        <div class="pv3 cf">
          <div class="fl w-70">
            <span class="black-80 b">{{ $msg->name }}</span>
            <span class="gray">&lt;{{ $msg->email }}&gt;</span>
          </div>
          <div class="fl w-30 tr">
            {{ date("d M Y, H:i", strtotime($msg->received_at)) }}
          </div>
        </div>
        <pre class="bg-white pa0 bw0 sans-serif">{{ $msg->body }}</pre>
      </div>
    @endforeach

    <div class="br3 bg-white ph3 pb3 ba b--moon-gray">
      <div class="pv3">
        <i class="glyphicon glyphicon-share-alt"></i>
        <span class="gray">{{ $message->email }}</span>
      </div>

      {!! Form::open(["url" => url("admin/message/$message->id/reply"), "method" => "POST"]) !!}
        <textarea name="body" class="b--white w-100" rows="5"></textarea>
        <hr class="mt0 mb3" />
        <button type="submit" class="btn btn-primary"> Send </button>
      {!! Form::close() !!}
    </div>
  </div>
@endsection
