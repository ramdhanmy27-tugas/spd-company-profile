@extends("admin.app")

<?php

use App\Models\Message;

?>

@section("page-title")
  Pesan @if ($new_msg_count > 0) <span class="badge">{{ $new_msg_count }}</span> @endif
@endsection

@section("content")
  @if(count($data) > 0)
    <div class="mb4 tr cf" style="margin-top: -62px">
      <a href="{{ config("contact.mail.host") }}" class="btn btn-default fr ml2" target="_blank">
        <i class="glyphicon glyphicon-envelope"></i> Mail
      </a>
      @if ($new_msg_count > 0)
        <a href="{{ url("admin/message/read-all") }}" class="btn btn-default fr ml2" target="_blank">
          <i class="glyphicon glyphicon-folder-open"></i> Baca Semua
        </a>
      @endif
    </div>

    <div class="i silver f4 tr" style="margin-top: -10px">{{ $data->total() }} Pesan</div>

    @foreach($data as $message)
      <a
        href="{{ url("admin/message/$message->id") }}"
        class="black-60 cf db mt3 pv2 ph3 br2 ba b--moon-gray shadow-hover {{ $message->is_read ? "bg-black-10" : "bg-white" }}"
      >
        <div class="row">
          <div class="col-xs-3">
            <div class="f3">{{ $message->name }}</div>
            <div class="silver f4">{{ $message->email }}</div>
          </div>
          <div class="col-xs-7">
            <div class="b">{{ $message->subject }}</div>
            <div class="gray">
              {{ substr($message->body, 0, 100) }}
            </div>
          </div>
          <div class="col-xs-2 tr">
            <span class="silver f4">{{ date("H:i d M Y", strtotime($message->received_at)) }}</span>
          </div>
        </div>
      </a>
    @endforeach

    {{ $data->links() }}
  @else
    @include("utils.empty")
  @endif
@endsection
