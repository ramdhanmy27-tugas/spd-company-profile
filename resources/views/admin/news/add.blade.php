@extends("admin.app")

@section("page-title", "Tambah Berita")

@section("content")
  {!! Form::model([], ["url" => url("admin/news"), 'files' => true]) !!}
    <div class="mb4 tr" style="margin-top: -62px">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <a href="{{ url("admin/news") }}" class="btn btn-default">
        <i class="glyphicon glyphicon-share-alt"></i> Kembali
      </a>
    </div>

    @include("admin.news.form", get_defined_vars())
  {!! Form::close() !!}
@endsection
