<?php

use App\Models\News;
$has_model = $model instanceof News;

?>


<div class="row">
  <div class="col-sm-8">
    <div class="form-group">
      {!! Form::label("title", News::label("title"), ["class" => "col-form-label"]) !!}
      {!! Form::text("title", null, ["class" => "form-control"]) !!}
    </div>
    <div class="form-group">
      {!! Form::label("content", News::label("content"), ["class" => "col-form-label"]) !!}
      <div class="bg-white">
        <div id="editor" style="height: 300px">
          @if ($has_model) {!! $model->content !!} @endif
        </div>
        <input id="news-content" type="hidden" name="content" />
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      {!! Form::label("cover", News::label("cover"), ["class" => "col-form-label"]) !!}
      {!! Form::file("cover", null, ["class" => "form-control"]) !!}

      @if ($has_model && $model->cover !== null)
        <div class="dt mt2">
          <div class="thumbnail fixed-thumbnail-md">
            <img src="{{ $model->cover_url }}" style="max-height: 100%; max-width: 100%" />
          </div>
        </div>
      @endif
    </div>
  </div>
</div>

@push("css")
  <link href="{{ url("js/quill/quill.snow.min.css") }}" rel="stylesheet" type="text/css" />
@endpush

@push("js")
  <script type="text/javascript" src="{{ url("js/quill/quill.min.js") }}"></script>
  <script type="text/javascript" src="{{ url("js/quill/modules/image-drop.min.js") }}"></script>
  <script type="text/javascript" src="{{ url("js/quill/modules/image-resize.min.js") }}"></script>
  <script type="text/javascript">
    var quill = new Quill('#editor', {
      theme: 'snow',
      modules: {
        imageDrop: true,
        imageResize: { displaySize: true },
      }
    });

    $("form").submit(function() {
      $("#news-content").val($("#editor .ql-editor").html());
    });
  </script>
@endpush
