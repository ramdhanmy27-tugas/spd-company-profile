@extends("admin.app")

<?php

use App\Models\News;

?>

@section("page-title", "Berita")

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("admin/news/create") }}" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i> Tambah
    </a>
  </div>

  @if(count($data) > 0)
    @foreach($data as $item)
      <div class="br2 ba b--moon-gray pa3 bg-white shadow-hover mb3">
        <div class="row">
          <div class="col-sm-10">
            <div class="cf">
              <div class="fl">
                <div class="thumbnail fixed-thumbnail-sm">
                  <img src="{{ $item->getCoverUrlAttribute("sm") }}" />
                </div>
              </div>
              <div class="fl w-80 pl3">
                <div class="b f3">{{ $item->title }}</div>
                <div class="word-wrap">{{ substr(strip_tags($item->content), 0, 200) }}</div>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <action href="{{ url("admin/news/$item->id/edit") }}" cls="btn btn-sm btn-info mh1">
              <i class="glyphicon glyphicon-edit white"></i> Edit
            </action>
            <action href="{{ url("admin/news/$item->id") }}"
              cls="btn btn-sm btn-danger mh1"
              method="DELETE"
              onClick="return confirm('Anda yakin ?')"
            >
              <i class="glyphicon glyphicon-remove white"></i> Delete
            </action>
          </div>
        </div>
      </div>
    @endforeach

    {{ $data->links() }}
  @else
    @include("utils.empty")
  @endif
@endsection
