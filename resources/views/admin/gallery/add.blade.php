@extends("admin.app")

@section("page-title", "Tambah Galeri")

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("admin/gallery") }}" class="btn btn-default">
      <i class="glyphicon glyphicon-share-alt"></i> Kembali
    </a>
  </div>

  {!! Form::model([], ["url" => url("admin/gallery"), 'files' => true]) !!}
    @include("admin.gallery.form", get_defined_vars())
  {!! Form::close() !!}
@endsection
