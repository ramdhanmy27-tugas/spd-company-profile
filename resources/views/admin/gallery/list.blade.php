@extends("admin.app")

<?php

use App\Models\Gallery;

?>

@section("page-title", "Galeri")

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("admin/gallery/create") }}" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i> Tambah
    </a>
  </div>

  @if(count($data) > 0)
    @foreach($data as $item)
      <div class="br2 ba b--moon-gray pa3 bg-white shadow-hover mb3">
        <div class="row">
          <div class="col-sm-10">
            <div class="cf">
              <div class="fl">
                <div class="thumbnail ma0">
                  <div class="img-thumb bg-white shadow-hover" style="height: 100px; width: 100px">
                    <img src="{{ $item->getImageUrlAttribute("sm") }}" />
                  </div>
                </div>
              </div>
              <div class="fl w-80 pl3">
                <div class="word-wrap">{{ substr(strip_tags($item->description), 0, 200) }}</div>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <action href="{{ url("admin/gallery/$item->id/edit") }}" cls="btn btn-sm btn-info mh1">
              <i class="glyphicon glyphicon-edit white"></i> Edit
            </action>
            <action href="{{ url("admin/gallery/$item->id") }}"
              cls="btn btn-sm btn-danger mh1"
              method="DELETE"
              onClick="return confirm('Anda yakin ?')"
            >
              <i class="glyphicon glyphicon-remove white"></i> Delete
            </action>
          </div>
        </div>
      </div>
    @endforeach

    {{ $data->links() }}
  @else
    @include("utils.empty")
  @endif
@endsection
