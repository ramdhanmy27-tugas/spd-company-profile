<?php

use App\Models\Gallery;

?>

<div class="form-group row">
  {!! Form::label("image", Gallery::label("image"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    {!! Form::file("image", null, ["class" => "form-control"]) !!}

    @if ($model instanceof Gallery && $model->image !== null)
      <div class="dt mt2">
        <div class="thumbnail fixed-thumbnail-md">
          <img src="{{ $model->image_url }}" style="max-height: 100%; max-width: 100%" />
        </div>
      </div>
    @endif
  </div>
</div>

<div class="form-group row">
  {!! Form::label("description", Gallery::label("description"), [
    "class" => "col-form-label col-sm-2 tr",
  ]) !!}

  <div class="col-sm-6">
    {!! Form::textarea("description", null, ["class" => "form-control"]) !!}
  </div>
</div>

<div class="form-group row">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-secondary">Simpan</button>
  </div>
</div>
