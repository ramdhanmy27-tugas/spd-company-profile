<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title')</title>

    <link href="{{ asset("css/app.css") }}" rel="stylesheet" type="text/css" />
</head>
<body>
  <div class="center w-30 mt6">
    <div class="tc mv5">
      <img src="{{ asset("img/logo.png") }}" />
    </div>

    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}

      <div class="mb4 {{ $errors->has('email') ? ' has-error' : '' }}">
        <div class="input-group">
          <span class="input-group-addon">
            <i class="glyphicon glyphicon-user"></i>
          </span>
          <input
            id="email"
            type="text"
            class="form-control"
            name="email"
            value="{{ old('email') }}"
            placeholder="Email"
            required
            autofocus
          >
        </div>

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>

      <div class="mb4 {{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="input-group">
          <span class="input-group-addon">
            <i class="glyphicon glyphicon-lock"></i>
          </span>
          <input
            id="password"
            type="password"
            class="form-control"
            name="password"
            placeholder="Password"
            required
          >
        </div>

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>

      <div class="mb4">
        <button type="submit" class="btn btn-primary w-100">
          Sign In
        </button>
      </div>

      {{-- <div class="checkbox tc">
        <label>
          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Keep Me Signed In
        </label>
      </div> --}}
    </form>
  </div>
</body>
</html>
