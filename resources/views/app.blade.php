@extends("layout")

@section("body")
<div class="mt5 pt3">
{{-- <div> --}}
  {{-- Header --}}
  <nav id="header" class="navbar navbar-default navbar-fixed-top ma0 br0 shadow-1">
    <div class="container-fluid">
      <div class="navbar-header ">
        <a class="navbar-brand" href="{{ url("/") }}">
          <img src="{{ asset("img/logo.png") }}" height="25px" />
        </a>

        {{-- Navbar Toggle --}}
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      {{-- Header Menu --}}
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{ url("/#section-home") }}"><b>Home</b></a></li>
          {{-- <li><a href="#section-our-services"><b>Services</b></a></li>
          <li><a href="#section-about-us"><b>About Us</b></a></li>
          <li><a href="#section-our-partners"><b>Partners & Clients</b></a></li>
          <li><a href="#section-our-team"><b>Team</b></a></li> --}}
          <li><a href="{{ url("news") }}"><b>News</b></a></li>
          <li><a href="{{ url("gallery") }}"><b>Gallery</b></a></li>
          <li><a href="{{ url("/#section-contact") }}"><b>Contact Us</b></a></li>
          <li class="pv2 pl5 pr4 collapse hidden-sm hidden-xs">
            <a href="{{ url("/#section-spd-group") }}" style="padding: 0">
              <img src="{{ asset("img/logo/spd.png") }}" />
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  {{-- Content --}}
  <div id="content relative">
    <div class="fixed right-0 bottom-0 mh4 w-50 z-max">
      @if(Session::has("msg"))
        <div class="mt3">
          @foreach(Session::pull("msg") as $variant => $messages)
            <div class="alert alert-{{ $variant }} alert-dismissible shadow-1" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>

              <div class="list">
                @foreach($messages as $message)
                  <li>{{ $message }}</li>
                @endforeach
              </div>
            </div>
          @endforeach
        </div>
      @endif

      @if ($errors->any())
        <div class="alert alert-danger alert-dismissible show shadow-1" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

          <p class="pa0 ma0 b">Terdapat kesalahan:</p>
          {!! implode('', $errors->all('<li class="f4">:message</li>')) !!}
        </div>
      @endif
    </div>


    <div class="container-fluid">
      @hasSection("page-title")
        <h1 class="pb2 bb b--moon-gray ">
          @yield("page-title")
        </h1>
      @endif

      @yield("content")
    </div>
  </div>

  {{-- Content --}}
  {{-- <footer id="footer" class="bg-black-90 pv4 white">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4">
          <h4 class="b">
            <div class="ttu">PT. Sarana Primadata</div>
            <div>(SPD Group)</div>
          </h4>

          <div class="mb3 b">
            <div>Surapati Core</div>
            <div>Blok K-18, F-29, C-26</div>
            <div>Bandung 40124 Indonesia</div>
          </div>

          <div class="mb3 b">
            <div><a href="#" class="white">admin@saranaprimadata.co.id</a></div>
            <div>022 - 872 241 349</div>
          </div>

          <ul class="list">
            <li class="b">Geospatial Dynamic Modelling</li>
            <li class="b">Service Oriented Enterprise Data Analytic</li>
            <li class="b">Data & Statistic Analytic Services</li>
            <li class="b">Geospatial Information Services</li>
            <li class="b">Asset Management Services</li>
            <li class="b">Training & Special Purpose Event</li>
          </ul>
        </div>

        <div class="col-sm-8">
          <div class="col-sm-3">
            <h4 class="b ttu">Explore</h4>
            <ul class="list pa0">
              <li> <a href="{{ url("/") }}" class="b white">Home</a> </li>
              <li> <a href="{{ url("home") }}" class="b white">Services & Products</a> </li>
              <li> <a href="{{ url("home") }}" class="b white">About Us</a> </li>
              <li> <a href="{{ url("home") }}" class="b white">Our Team</a> </li>
              <li> <a href="{{ url("home") }}" class="b white">Our Values</a> </li>
              <li> <a href="{{ url("home") }}" class="b white">News</a> </li>
              <li> <a href="{{ url("home") }}" class="b white">Publication</a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer> --}}
</div>
@endsection
