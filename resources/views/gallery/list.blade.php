@extends("app")

<?php

use App\Models\Gallery;

?>

@section("page-title")
  <i class="glyphicon glyphicon-picture"></i> Gallery
@endsection

@section("content")
  @if(count($data) > 0)
    <div class="cf gallery mt4">
      @foreach($data as $item)
        <figure class="fl w-20-l w-30-m w-50 ph2">
          <a href="{{ $item->image_url }}" data-size="0x0">
            <div class="thumbnail">
              <div class="img-thumb bg-white shadow-hover" style="height: 200px">
                <img src="{{ $item->getImageUrlAttribute("sm") }}" alt="{{ $item->description }}" />
              </div>
            </div>
          </a>
          <figcaption class="hide">
            <div class="tc">{{ $item->description }}</div>
          </figcaption>
        </figure>
      @endforeach
    </div>

    {{ $data->links() }}

    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
  @else
    @include("utils.empty")
  @endif
@endsection

@push("css")
  <link rel="stylesheet" href="{{ asset("js/photoswipe/photoswipe.min.css") }}">
  <link rel="stylesheet" href="{{ asset("js/photoswipe/default-skin.min.css") }}">
@endpush

@push("js")
  <script src="{{ asset("js/photoswipe/photoswipe.min.js") }}"></script>
  <script src="{{ asset("js/photoswipe/photoswipe-ui-default.min.js") }}"></script>
  <script src="{{ asset("js/photoswipe/gallery.js") }}"></script>
  <script type="text/javascript">
    initPhotoSwipeFromDOM('.gallery');
  </script>
@endpush
