@extends("app")

@section("page-title", "Riwayat Peminjaman")

@section("content")
  @if (count($peminjaman) > 0)
    @each("peminjaman.item", collect($peminjaman->all())->map(function($item) {
      $item->reqReturnUrl = url("operator/returnTools/$item->kode_peminjaman");
      $item->detailUrl = url("operator/history/$item->kode_peminjaman");

      return $item;
    }), "peminjaman")

    <div class="mv3">
      {{ $peminjaman->links() }}
    </div>
  @else
    <div class="pa4 tc black-50 f2">
      <i class="glyphicon glyphicon-exclamation-sign"></i> Kosong
    </div>
  @endif
@endsection
