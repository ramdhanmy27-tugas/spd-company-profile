@extends("app")

<?php

use App\Models\Peminjaman;

?>

@section("page-title")
  Peminjaman <span class="text-primary">{{ $peminjaman->kode_peminjaman }}</span>
@endsection

@section("content")
  <div class="mb4 tr" style="margin-top: -62px">
    <a href="{{ url("operator/history") }}" class="btn btn-default">
      <i class="glyphicon glyphicon-share-alt"></i> Kembali
    </a>
  </div>

  @include("peminjaman.detail", compact("peminjaman"))
@endsection
