@extends("app")

<?php

use App\Models\News;

$searchQuery = Request::input("search");

?>

@section("page-title")
  <i class="glyphicon glyphicon-list-alt"></i> News
@endsection

@section("content")
  @if (count($newest) > 0)
    <div class="row">
      <div class="col-md-6 ph5 br b--moon-gray">
        <div class="news-slider">
          @foreach($newest as $item)
            <div class="relative img-thumb pa0" style="width: 100%; height: 400px; padding: 0">
              <a href="{{ url("news/$item->seoId") }}">
                <img data-lazy="{{ $item->cover_url }}" />
                <div class="absolute z-max bg-black-80 ph4 pt3 pb4 w-100 left-0 shadow-1 bottom-0">
                  <div class="white b f3">{{ $item->title }}</div>
                  <div class="light-gray f4 lh-title">{{ $item->short_content }}</div>
                </div>
              </a>
            </div>
          @endforeach
        </div>
      </div>
      <div class="col-md-6">
        <div class="pb3 bb b--moon-gray mt2">
          {{ Form::open(["method" => "GET"]) }}
            <div class="input-group">
              <input
                type="text"
                name="search"
                class="form-control"
                placeholder="Pencarian"
                value="{{ $searchQuery }}"
              />
              <span class="input-group-btn">
                @if (strlen($searchQuery) > 0)
                  <a href="{{ url("news") }}" class="btn btn-default">
                    <i class="glyphicon glyphicon-remove"></i>
                  </a>
                @endif
                <button class="btn btn-default" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </span>
            </div>
          {{ Form::close() }}
        </div>

        @foreach($data as $item)
          <a href="{{ url("news/$item->seoId") }}" class="bb b--moon-gray db pa3">
            <div class="b f3">{{ $item->title }}</div>
            <div class="word-wrap gray f4">
              {{ $item->getShortContentAttribute(80) }}
            </div>
          </a>
        @endforeach

        <div class="tc">
          {{ $data->links() }}
        </div>
      </div>
    </div>
  @else
    @include("utils.empty")
  @endif
@endsection

@push("css")
  <link rel="stylesheet" href="{{ asset("js/slick/slick.css") }}">
  <link rel="stylesheet" href="{{ asset("js/slick/slick-theme.css") }}">
@endpush

@push("js")
  <script src="{{ asset("js/slick/slick.min.js") }}"></script>
  <script type="text/javascript">
    $('.news-slider').slick({
      lazyLoad: 'ondemand',
      dots: true,
      autoplay: true,
    });
  </script>
@endpush
