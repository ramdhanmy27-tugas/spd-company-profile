<div class="br2 ba b--moon-gray pa3 bg-white shadow-hover">
  <div class="row">
    <div class="col-sm-3">
      <div class="img-thumb img-thumb-sm">
        <img src="{{ $news->getCoverUrlAttribute("sm") }}" />
      </div>
    </div>
    <div class="col-sm-6">
      <p class="b f3">{{ $news->title }}</p>
      <div>{{ substr(strip_tags($news->content), 0, 100) }}</div>
    </div>
    <div class="col-sm-3">
      <action href="{{ url("admin/news/$news->id/edit") }}" cls="btn btn-sm btn-info mh1">
        <i class="glyphicon glyphicon-edit white"></i> Edit
      </action>
      <action href="{{ url("admin/news/$news->id") }}"
        cls="btn btn-sm btn-danger mh1"
        method="DELETE"
        onClick="return confirm('Anda yakin ?')"
      >
        <i class="glyphicon glyphicon-remove white"></i> Delete
      </action>
    </div>
  </div>
</div>
