@extends("app")

<?php

use App\Models\News;

?>

@section("content")
  <div class="row bb b--moon-gray mv3 mh0 pb3">
    <div class="col-xs-8 col-md-10">
      <h3 class="ma0">{{ $model->title }}</h3>
    </div>
    <div class="col-xs-4 col-md-2 dtc v-btm">
      <div class="tr">
        <div class="gray">
          {{ $model->user->name }}
        </div>
        <div class="gray i f4">
          {{ date("H:i - d M Y", strtotime($model->created_at)) }}
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-4 fr-m fr-l">
      <div class="thumbnail bg-white">
        <img src="{{ $model->coverUrl }}" />
      </div>
    </div>
    <div class="col-sm-8 br b--moon-gray" style="min-height: 400px">
      <div class="table-responsive">
        {!! $model->content !!}
      </div>
    </div>
  </div>
@endsection
