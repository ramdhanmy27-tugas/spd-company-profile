<html>
  <head>
    <style type="text/css">
      * {
        font-family: sans-serif;
      }
      p {
        color: #777777;
      }
      body {
        background: #eee;
        font-size: 14px;
      }
      .main {
        background: #fff;
        color: #333;
        border: 1px solid #ccc;
        border-radius: 4px;
        overflow: hidden;
        padding: 20px 20px 30px;
        margin: 30px auto 20px;
        width: 800px;
      }
    </style>
  </head>
  <body>
    <div class="main">
      @yield("content")
    </div>
  </body>
</html>
