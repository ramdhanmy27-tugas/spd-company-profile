@extends("email.app")

@section("content")
  <div style="text-align: left;">
    <div style="font-weight: bold; font-size: 18px">{{ $msg->subject }}</div>
    <div>{{ $msg->name }}</div>
    <div style="font-size: 12px; color: #aaa">
      <a href="mailto:{{ $msg->email }}" target="_blank">
        <i>{{ $msg->email }}</i>
      </a>
    </div>
  </div>

  <hr />
  <pre style="color: #777">{{ $msg->body }}</pre>
@endsection
