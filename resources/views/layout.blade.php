<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="base-url" content="{{ url("/") }}/">
  <link href="{{ asset("favicon.ico") }}" rel="shortcut icon" />

  <title>@hasSection('title') @yield('title') @else PT Sarana Primadata @endif</title>

  <link href="{{ asset("css/app.css") }}" rel="stylesheet" type="text/css" />
  @stack("css")
</head>
<body>
  <div id="app">
    @yield("body")
  </div>

  <script src="{{ asset("js/app.js") }}" type="text/javascript"></script>
  @stack("js")
</body>
</html>
