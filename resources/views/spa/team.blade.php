<?php

$team = [
  [
    "name" => "Ir. Prihyono AL. P.",
    "position" => "President Director & CEO",
    "photo" => "foto/pri.png",
  ],
  [
    "name" => "Ir. Hardi Koeslamwardi, MSE",
    "position" => "Main Commissioner",
    "photo" => "foto/hardi.png",
  ],
  [
    "name" => "Ir. Dodi Sukmayadi, MSE",
    "position" => "President Director & CEO",
    "photo" => "foto/dodi.png",
  ],
  [
    "name" => "Ir. Suluh Tjiptadi, MT",
    "position" => "Commissioner",
    "photo" => "foto/suluh.png",
  ],
  [
    "name" => "Muhammad Sofwan, ST",
    "position" => "VP Geospatial Information Technology",
    "photo" => "foto/sofwan.png",
  ],
  [
    "name" => "Triaji, SH",
    "position" => "Managing Director & CFO",
    "photo" => "foto/triaji.png",
  ],
  [
    "name" => "Hendra C. Suparmo, SE, AK",
    "position" => "VP Enterprise Solution",
    "photo" => "foto/hendra.png",
  ],
  [
    "name" => "Bagus Indrawan H, S.Si, MT",
    "position" => "Lead Modeller",
    "photo" => "foto/bagus.png",
  ],
  [
    "name" => "M. Faruk Rosyaridho, S.T.,",
    "position" => "Technical Director",
    "photo" => "foto/faruk.png",
  ],
];

?>

<div class="mv4">
  <h4 class="ttu b">Our Team & Expert</h4>

  <div class="bg-black-10 pa4">
    <div class="row">
      @foreach ($team as $people)
        <div class="col-md-2 col-sm-4 col-xs-6 pb4">
          <div class="bg-white relative shadow-hover">
            <div class="overflow-hidden tc" style="height: 250px">
              <img src="{{ asset("img/$people[photo]") }}" style="min-width: 100%; object-fit: cover; object-position: top; height: 80%" />
            </div>

            <div class="w-100 bg-black pa2 white absolute bottom-0 f6 f4-m f5-l">
              <div class="b">{{ $people["name"] }}</div>
              <div>{{ $people["position"] }}</div>
              <div>SPD Group</div>
            </div>
          </div>
        </div>
      @endforeach
    </div>

    <div class="pv2 f2-l f2-m f3">
      "<b>In our service business, our people are the brand.</b>
      <span class="text-primary">We want to invest in people and processes to support it</span>"
    </div>
  </div>
</div>
