<div class="row bg-orange">
  <div class="ph7-l ph4-m pv5 ph3" style="background: url('{{ asset("img/cover.jpg")  }}')">
    <div class="shadow-1 bg-white ph4 pv2">
      <h4 class="b">SPD Group</h4>

      <div class="tc pv4">
        <img src="{{ asset("img/logo/spd.png") }}" height="40px" />
      </div>
      <div class="tc pv4">
        <img src="{{ asset("img/logo.png") }}" height="25px" />
      </div>
      <div class="tc pv4">
        <img src="{{ asset("img/logo/mulya.png") }}" height="40px" class="ph4 pv2-m pv1-l pv3" />
        <img src="{{ asset("img/logo/wikom.png") }}" height="40px" class="ph4 pv2-m pv1-l pv3" />
        <img src="{{ asset("img/logo/msm.png") }}" height="40px" class="ph4 pv2-m pv1-l pv3" />
        <img src="{{ asset("img/logo/swik.png") }}" height="40px" class="ph4 pv2-m pv1-l pv3" />
        <img src="{{ asset("img/logo/swik2.png") }}" height="40px" class="ph4 pv2-m pv1-l pv3" />
      </div>
    </div>
  </div>
</div>
