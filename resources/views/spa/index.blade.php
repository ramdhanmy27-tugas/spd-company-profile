@extends("app")

@section("content")
  <div id="section-home" class="relative z-2 section">
    @include("spa.home")
  </div>

  <div id="section-about-us" class="section">
    @include("spa.about-us")
  </div>

  <div id="section-our-partners" class="bg-white row pv3 section">
    <div class="col-xs-12">
      @include("spa.partners")
    </div>
  </div>

  <div id="section-our-team" class="section">
    @include("spa.team")
  </div>

  <div id="section-values" class="section">
    @include("spa.values")
  </div>

  <div id="section-contact" class="section">
    @include("spa.contact")
  </div>

  <div id="section-spd-group" class="section">
    @include("spa.spd-group")
  </div>
@endsection

@push("css")
  <link rel="stylesheet" href="{{ asset("js/slick/slick.css") }}">
  <link rel="stylesheet" href="{{ asset("js/slick/slick-theme.css") }}">
@endpush

@push("js")
  <script src="{{ asset("js/slick/slick.min.js") }}"></script>
@endpush
