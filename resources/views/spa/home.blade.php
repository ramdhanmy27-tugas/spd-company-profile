<div class="service-slider row overflow-hidden bg-white" style="margin-bottom: 0">
  <div class="relative">
    {{-- Home --}}
    <div class="img-thumb" style="height: 450px">
      <img src="{{ asset("img/background/cover.png") }}" />
    </div>
    {{-- <div class="absolute left-0 pv3 ph4 w-50 black-80 tr lh-solid" style="top: 40%">
      <div class="text-primary" style="font-size: 200%">SPD Group</div>
      <div class="b" style="font-size: 350%">SARANA PRIMADATA</div>
      <div class="text-primary i" style="font-size: 200%">Data Moving Ahead</div>
    </div> --}}
  </div>

  {{-- Data Statistic & Analytic --}}
  <div class="relative">
    <div class="img-thumb" style="height: 450px">
      <img src="{{ asset("img/background/statistic.jpg") }}" />
    </div>
    <div class="absolute top-2 left-0 tr f3-l f3-m f6 b pv3 ph4 w-80 w-70-m w-60-l white-80 bg-black-80 shadow-1 tj">
      We provide integrated data analysis & statistical solutions for government and corporate statistics based on world standards on GSBPM / GSIM, SDMX and DDI. Our services will leverage your Big Data and Open Data initiatives to leverage the value of information for the executive decision-making process.
    </div>
  </div>

  {{-- Asset Management --}}
  <div class="relative">
    <div class="img-thumb" style="height: 450px">
      <img src="{{ asset("img/background/asset-management.jpg") }}" />
    </div>
    <div class="absolute bottom-2 right-0 tr f3-l f3-m f6 b pv3 ph4 w-80 w-70-m w-60-l white-80 bg-black-80 shadow-1 tj">
      Showcase your team, products, clients, about info, testimonials, latest posts from the blog, contact form, additional calls to action. Everything translation ready.
    </div>
  </div>

  {{-- Geospatial Information --}}
  <div class="relative">
    <div class="img-thumb" style="height: 450px">
      <img src="{{ asset("img/background/geospatial.png") }}" />
    </div>
    <div class="absolute top-2 right-0 tr f3-l f3-m f6 b pv3 ph4 w-80 w-70-m w-60-l white-80 bg-black-80 shadow-1 tj">
      We improve your geospatial information assets to provide a holistic view of your resources. Our services are in the area of geo analytical through remote sensing, satellite image interpretation, terrestrial, hydrographic, airborne surveying and mapping, with global positioning system (GPS).
    </div>
  </div>

  {{-- Training --}}
  <div class="relative">
    <div class="img-thumb" style="height: 450px">
      <img src="{{ asset("img/background/training.jpg") }}" />
    </div>
    <div class="absolute bottom-2 left-0 tr f3-l f3-m f6 b pv3 ph4 w-80 w-70-m w-60-l white-80 bg-black-80 shadow-1 tj">
      We undertake special-purpose activities, including training in human resource analysis, GIS, spatial databases, remote sensing, global positioning systems, survey mapping, information systems, and trainer training. We also organize our special annual event named DATA SUMMIT – TECHNOCONFERENCE & EXPO.
    </div>
  </div>
</div>

<div class="row bg-white shadow-1 relative z-5">
  <div class="col-xs-3 pv3 pointer dim service-slider-control" data-idx="1">
    <div class="tc pa3 pb3">
      <img src="{{ asset("img/icon/data-statistic-analytic.png") }}" />
    </div>
    <p class="b tc ttu hidden-xs hidden-sm">Data & Statistic Analytics <br /> Product & Services</p>
  </div>
  <div class="col-xs-3 pv3 pointer dim service-slider-control" data-idx="2">
    <div class="tc pa3 pb3">
      <img src="{{ asset("img/icon/asset-management.png") }}" />
    </div>
    <p class="b tc ttu hidden-xs hidden-sm">Assets Management <br /> Product & Services</p>
  </div>
  <div class="col-xs-3 pv3 pointer dim service-slider-control" data-idx="3">
    <div class="tc pa3 pb3">
      <img src="{{ asset("img/icon/geospatial-information.png") }}" />
    </div>
    <p class="b tc ttu hidden-xs hidden-sm">Geospatial Information <br /> Product & Services</p>
  </div>
  <div class="col-xs-3 pv3 pointer dim service-slider-control" data-idx="4">
    <div class="tc pa3 pb3">
      <img src="{{ asset("img/icon/training-service.png") }}" />
    </div>
    <p class="b tc ttu hidden-xs hidden-sm">Training & Special <br /> Purpose Events</p>
  </div>
</div>

@push("js")
  <script src="{{ asset("js/slick/slick.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var slider = $('.service-slider');

      slider.slick({
        lazyLoad: 'ondemand',
        dots: true,
        autoplay: true,
      });

      slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('[data-idx='+ currentSlide +']').removeClass('bg-moon-gray');
        $('[data-idx='+ nextSlide +']').addClass('bg-moon-gray');
      });

      $('.service-slider-control').click(function() {
        var $this = $(this);
        slider.slick('slickGoTo', $this.data("idx"));
      });
    });
  </script>
@endpush
