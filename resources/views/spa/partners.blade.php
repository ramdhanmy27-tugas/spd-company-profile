<h4 class="ttu b">Our Partners & Clients</h4>

<div class="ba b--moon-gray br2">
  <div class="pa3">
    <div class="ttu b f3 mb4">Our Clients</div>

    <div class="row ph3">
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/big.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/bpn.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/bpbatam.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/bappenas.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/danida.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/ddi.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/listrik.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
      <div class="col-md-2 col-xs-4" style="height: 130px">
        <div class="h-100 v-mid tc pa3">
          <img src="{{ asset("img/logo/wb.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
    </div>
  </div>

  <div class="row ma0">
    <div class="col-md-2 bg-black-90 pa4">
      <div class="ttu b f2-l white">Our Partners</div>
      {{-- <a href="#" class="white">+ Learn More</a> --}}
    </div>

    <div class="col-md-10 bt b--moon-gray">
      <div class="dt w-100">
        <div class="dtc v-mid tc pa3">
          <img src="{{ asset("img/logo/agilis.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
        <div class="dtc v-mid tc pa3">
          <img src="{{ asset("img/logo/esri.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
        <div class="dtc v-mid tc pa3">
          <img src="{{ asset("img/logo/fis.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
        <div class="dtc v-mid tc pa3">
          <img src="{{ asset("img/logo/sdmx.png") }}" style="max-width: 100%; max-height: 100%" />
        </div>
      </div>
    </div>
  </div>
</div>
