<h4 class="ttu b mt4">About Us</h4>

<div class="relative overflow-hidden" style="height: 500px; background: url('{{ asset('img/background/building.jpg') }}')">
  <div class="absolute-m absolute-l bg-white-80 pa4 w-100 top-0 tj">
    Founded in 2001, PT. Sarana Primadata (SPD Group) is a company that is engaged in the field of Consultancy Services.
    Experienced technical & managerial, devices and adequate up hold our professionalism in every implementation of the work with Our Expert & Team Work Development Programs for Welfare Society, Nations and State.
  </div>

  <div class="absolute-m absolute-l bg-white-80 pa4 pt2 w-70-l bottom-0 right-0">
    <h4 class="b">Since 2014, SPD has begun transformed into Information Technology - Based Consulting Firm</h4>
    <p class="tj">
      We covered not only in the processing and management of spatial data, but also includes the development of database applications in its class, Enterprise Analytic & Statistics, and Data Warehouse & Business Intelligence.
      SPD offers a comprehensive package of services from solution architecture planning to big data management and deployment sustainment.
    </p>
  </div>
</div>

<div class="pv2">
  <span class="b f2-m f3-ns text-primary">Become a leading company</span>
  <span class="b f2-m f3-ns text-primary">in data solutions, analysis, and statistics</span>
</div>
