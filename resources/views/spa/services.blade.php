<div class="row bg-black-10">
  <div class="col-xs-4 bg-white pa4" style="min-height: 600px;">
    <h4 class="b">OUR SERVICES</h4>
    <h3 class="b text-primary">GEOSPATIAL INFORMATION <br /> PRODUCT & SERVICES</h3>
    <p class="b tj hidden-xs hidden-sm">
      We improve your geospatial information assets to provide a holistic view of your resources.
      Our services are in the area of geo analytical through remote sensing, satelite image interpretation, Terrestrial hydrographic, airborne surveying, and mapping, with global positioning system (GPS).
    </p>

    <div class="br2 bg-black-10 mb3">
      <div class="pa2 row">
        <div class="col-md-3 tc">
          <img src="{{ asset("img/icon/data-statistic-analytic.png") }}" height="50px" />
        </div>
        <div class="col-md-9 ttu pl0 b hidden-xs hidden-sm">
          Data & Statistic Analytics <br /> Product Services
        </div>
      </div>

      <div class="pa2 row">
        <div class="col-md-3 tc">
          <img src="{{ asset("img/icon/asset-management.png") }}" height="50px" />
        </div>
        <div class="col-md-9 ttu pl0 b hidden-xs hidden-sm">
          Assets Management <br /> Product Services
        </div>
      </div>

      <div class="pa2 row">
        <div class="col-md-3 tc">
          <img src="{{ asset("img/icon/geospatial-information.png") }}" height="50px" />
        </div>
        <div class="col-md-9 ttu pl0 b hidden-xs hidden-sm">
          Geospatial Information <br /> Product Services
        </div>
      </div>

      <div class="pa2 row">
        <div class="col-md-3 tc">
          <img src="{{ asset("img/icon/training-service.png") }}" height="50px" />
        </div>
        <div class="col-md-9 ttu pl0 b hidden-xs hidden-sm">
          Training & Special <br /> Purpose Events
        </div>
      </div>
    </div>

    <div class="tr">
      <button class="btn btn-default ttu">Next</button>
    </div>
  </div>

  <div class="col-xs-8 pt4 pl4">
    <div class="relative overflow-hidden" style="height: 550px;">
      <div style="height: 100%; background: url('{{ asset("img/background/geospatial.png") }}')"></div>

      <div class="absolute bottom-0 w-100 ph4 pv2 bg-white-80" style="box-shadow: 0 -1px 10px">
        <div class="pv2">
          <span class="b f3">Geospatial Information Product and Services</span>
          <span class="b f2 text-primary">3D City Modelling</span>
        </div>

        <p class="tj hidden-xs hidden-sm">
          3D city models are digital geospatial 3D models of urban areas that represent terrain surfaces, sites, buildings, vegetation, ingrastructure and landscape elements as well as related objects (e.g. city furniture) belonging to urband areas 3D city models support presentation, exploration, analysis, and management tasks in a large number of different application domains. In particular, 3D city models allow "for visually integrating hetero-geneous geo - information within a single framework and, therefore, create and manage complex urban information spaces".
        </p>
      </div>
    </div>
  </div>
</div>
