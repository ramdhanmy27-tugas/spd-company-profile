<div class="container">
  <div class="row pv4">
    <div class="col-sm-6 pv3 f3 fr-m fr-l">
      <div class="f2 b mb3">Connect with us:</div>

      <div class="mb3">
        <div>For support or any question, email us at</div>
        <a href="mailto:{{ config("mail.from.address") }}" target="_blank">
          <i class="glyphicon glyphicon-envelope"></i> admin@saranaprimadata.co.id
        </a>
      </div>

      <div class="mb3">
        <b>SPD Group</b>
        <div>Surapati Core Blok K-18, F-29, C-26</div>
        <div>Jl. PHH Mustofa (Suci) No. 39, Bandung 40214 Indonesia</div>
      </div>

      <div class="mb3">
        <div class="b">(022) 872-413-49 (Fax)</div>
        <div class="b">(022) 872-413-49</div>
        <div class="b">(022) 872-413-50</div>
      </div>
    </div>

    <div class="col-sm-6 pv3">
      <div class="ba shadow-1 b--moon-gray pa4 bg-white">
        {{ Form::open(["url" => url("send-message"), "method" => "POST"]) }}
          <div class="form-group">
            {!! Form::text("name", null, ["class" => "form-control input-sm", "placeholder" => "Your Name"]) !!}
          </div>
          <div class="form-group">
            {!! Form::text("email", null, ["class" => "form-control input-sm", "placeholder" => "Your Email"]) !!}
          </div>
          <div class="form-group">
            {!! Form::text("subject", null, ["class" => "form-control input-sm", "placeholder" => "Your Subject"]) !!}
          </div>
          <div class="form-group">
            {!! Form::textarea("body", null, ["class" => "form-control input-sm", "placeholder" => "Pesan", "rows" => 3]) !!}
          </div>
          <div class="tr">
            <button type="submit" class="btn btn-sm btn-success ttu">
              Send Message
            </button>
          </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
