<div class="row">
  <div class="ph6-l ph4 pv7-l pv5 pv6-m overflow-hidden" style="height: 550px; background: url('{{ asset("img/background/bg-values.png")  }}')">
    <div class="shadow-1 bg-white pa4">
      <h4 class="b ttu tc pb3">Our Values</h4>

      <div class="ph4 pt2 pb4 b f4 f3-l f3-m tj">
        <span class="b f2-l text-primary">We are ready and positioned to provide service solutions</span>, so we adhere to our strengths and stay focused on strategy.
        In out service business, our people are the brand, so we want to invest in people and processes to support it.
      </div>

      <div class="ph4 pt2 pb4 b f4 f3-l f3-m tj">
        <span class="b f2-l text-primary">Our customers are our priority</span> and we align our resources according to customer demand.
        In today's fastest world movement, we always keep our commitment to business and cultural change for the long term.
      </div>
    </div>
  </div>
</div>
