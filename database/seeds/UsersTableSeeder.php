<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name'           => 'Administrator',
            'email'          => 'admin@app.com',
            'password'       => 'admin',
        ]);
    }
}
